import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import React from 'react';


const errorStyle = {
  position: 'absolute',
  marginBottom: '-22px',
  color: 'red',
};
const UserDetailsForm = (props) => {
  const { values, handleChange, errors: {errors}} = props;
  
  return (
    <form>
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Enter User Details
      </Typography>
      <Grid container spacing={24}>
        <Grid item xs={12} sm={6}>
      
          <TextField
            required
            id="firstName"
            name="firstName"
            label="First name"
            fullWidth
            autoComplete="firstName"
            onChange={handleChange('firstName')}
            defaultValue={values.firstName}
            error={errors.isValid}
            helperText={errors.errors.firstName}
          />
         
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="lastName"
            name="lastName"
            label="Last name"
            fullWidth
            autoComplete="lastName"
            onChange={handleChange('lastName')}
            defaultValue={values.lastName}
            error={errors.isValid}
            helperText={errors.errors.lastName}
          />
        </Grid>
        <Grid item xs={12}>
   
          <TextField
            required
            id="email"
            name="email"
            label="Email Address"
            fullWidth
            autoComplete="email"
            onChange={handleChange('email')}
            defaultValue={values.email}
            error={errors.isValid}
            helperText={errors.errors.email}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            required
            id="streetAddr"
            name="streetAddr"
            label="Street Address"
            fullWidth
            autoComplete="user-details address street-addr"
            onChange={handleChange('streetAddr')}
            defaultValue={values.streetAddr}
            error={errors.isValid}
            helperText={errors.errors.streetAddr}
          />
        </Grid>
        {values.errors}
      </Grid>
    </React.Fragment>
    </form>
  );
}

export default UserDetailsForm;
