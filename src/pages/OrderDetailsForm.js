import Grid from '@material-ui/core/Grid';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import React from 'react';

const duration = [
  {
    value: '3',
    label: '3',
  },
  {
    value: '6',
    label: '6',
  },
  {
    value: '12',
    label: '12',
  },
];

const gigabytes = [
  {
    value: '3',
    label: '3',
  },
  {
    value: '5',
    label: '5',
  },
  {
    value: '10',
    label: '10',
  },
  {
    value: '20',
    label: '20',
  },
  {
    value: '30',
    label: '30',
  },
  {
    value: '50',
    label: '50',
  }
];

const upfrontPayment = [
  {
    value: 'true',
    label: 'Yes',
  },
  {
    value: 'false',
    label: 'No',
  },
];

const OrderDetailsForm = (props) => {
  const { values, handleChange, errors: {errors} } = props;
  console.log(errors)
  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Select subscription parameters:
      </Typography>
      <br/>
      <Grid container spacing={24}>
        <Grid item xs={12}>

        <TextField
          id="duration"
          select
          label="Duration"
          value={values.duration}
          onChange={handleChange('duration')}
          
          //helperText="Please select your duration in Months"
          margin="normal"
          variant="outlined"
          error={errors.isValid}
          helperText={errors.errors.duration}
        >
          {duration.map(option => (
            <MenuItem key={option.value} value={option.value}>
              {option.label}
            </MenuItem>
          ))}
        </TextField>
        <br/>
        
        </Grid>
        <Grid item xs={12}>
        <TextField
        id="gigabytes"
        select
        label="Gigabytes"
        value={values.gigabytes}
        onChange={handleChange('gigabytes')}
        
        //helperText="Please select the amount of gigabytes in a cloud"
        margin="normal"
        variant="outlined"
        error={errors.isValid}
        helperText={errors.errors.gigabytes}
      >
        {gigabytes.map(option => (
          <MenuItem key={option.value} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
      </TextField>
      <br/>
        </Grid>

        <Grid item xs={12}>
        <TextField
        id="upfrontPayment"
        select
        label="Upfront Payment"
        value={values.upfrontPayment}
        onChange={handleChange('upfrontPayment')}
        
        //helperText="Would you like to pay Upfront?"
        margin="normal"
        variant="outlined"
        error={errors.isValid}
        helperText={errors.errors.upfrontPayment}
      >
        {upfrontPayment.map(option => (
          <MenuItem key={option.value} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
      </TextField>
      <br/>
        </Grid>
        {values.errors}
      </Grid>
    </React.Fragment>
  );
}



export default OrderDetailsForm;
