const Validator = require('validator');
const isEmpty = require('./is-empty');

const validateUserDetailsInput = (data) => {
	let errors = {};

	data.firstName = !isEmpty(data.firstName) ? data.firstName : '';
	data.lastName = !isEmpty(data.lastName) ? data.lastName : '';
	data.email = !isEmpty(data.email) ? data.email : '';
	data.streetAddr = !isEmpty(data.streetAddr) ? data.streetAddr : '';
	
	
	
	// First Name Field Validation
	if (!Validator.isLength(data.firstName, { min: 2, max: 30 })) {
		errors.firstName = 'First name must be between 2 and 30 characters';
	}

	if (Validator.isEmpty(data.firstName)) {
		errors.firstName = 'First name field is required';
	}

	// Last Name Field Validation
	if (!Validator.isLength(data.lastName, { min: 2, max: 30 })) {
		errors.lastName = 'Last name must be between 2 and 30 characters';
	}

	if (Validator.isEmpty(data.lastName)) {
		errors.lastName = 'Last name field is required';
	}

	// Email Field Validation
	if (Validator.isEmpty(data.email)) {
		errors.email = 'Email field is required';
	}

	if (!Validator.isEmail(data.email)) {
		errors.email = 'Email is invalid';
	}

	// Street Address Field Validation
	if (!Validator.isLength(data.streetAddr, { min: 10, max: 300 })) {
		errors.streetAddr = 'Street address must be between 10 and 300 characters';
	}

	if (Validator.isEmpty(data.streetAddr)) {
		errors.streetAddr = 'Street address field is required';
	}

	


	return {
		errors,
		isValid: isEmpty(errors)
	};
};

const validatePaymentDetailsInput = (data) => {
	let errors = {};

	data.creditNumber = !isEmpty(data.creditNumber) ? data.creditNumber : '';
	data.expDate = !isEmpty(data.expDate) ? data.expDate : '';
	data.cvv = !isEmpty(data.cvv) ? data.cvv : '';

	// Credit Card Number Field Validation
	if (Validator.isCreditCard(data.creditNumber)) {
		errors.creditNumber = 'Credit card number invalid';
	}
	if (Validator.isEmpty(data.creditNumber)) {
		errors.creditNumber = 'Credit Card field is required';
	}
	
	// Credit expire date Field Validation
	if (Validator.isEmpty(data.expDate)) {
		errors.expDate = 'Credit Card expire date field is required';
	}

	// Credit Card security code Field Validation
	if (!Validator.isNumeric(data.cvv)) {
		errors.cvv = 'Credit Card security code field should be Numeric';
	}
	if (Validator.isEmpty(data.cvv)) {
		errors.cvv = 'Credit Card security code field is required';
	}


	return {
		errors,
		isValid: isEmpty(errors)
	};
}

const validateOrderDetailsInput = (data)  => {
	let errors = {};


	data.duration = !isEmpty(data.duration) ? data.duration : '';
	data.gigabytes = !isEmpty(data.gigabytes) ? data.gigabytes : '';
	data.upfrontPayment = !isEmpty(data.upfrontPayment) ? data.upfrontPayment : '';
	
		// Subscription Duration Field Validation
		if (!Validator.isNumeric(data.duration)) {
			errors.duration = 'Subscription duration field should be Numeric';
		}
	
		if (Validator.isEmpty(data.duration)) {
			errors.duration = 'Subscription duration field is required';
		}
	
		// Gigabytes Field Validation
		if (!Validator.isNumeric(data.gigabytes)) {
			errors.gigabytes = 'Gigabytes field should be Numeric';
		}
		
		if (Validator.isEmpty(data.gigabytes)) {
			errors.gigabytes = 'Gigabytes field is required';
		}
	
		// Upfront payment Field Validation
		if (!Validator.isBoolean(data.upfrontPayment)) {
			errors.upfrontPayment = 'Upfront payment field should be true/false';
		}
	
		if (Validator.isEmpty(data.upfrontPayment)) {
			errors.upfrontPayment = 'Upfront payment field is required';
		}
	
	return {
		errors,
		isValid: isEmpty(errors)
	};
}

module.exports = {
	validateOrderDetailsInput,
	validatePaymentDetailsInput,
	validateUserDetailsInput
}